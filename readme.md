

To connect to a ESP8266 board with 74880 baudrate use miniterm.py:
    sudo miniterm.py /dev/ttyUSB0 74880

Flash fw with rboot:
    esptool.py --port /dev/ttyUSB0 --baud 74880 write_flash -fs 32m -fm qio -ff 40m 0x0 ./esp-open-rtos/bootloader/firmware_prebuilt/rboot.bin 0x1000 ./esp-open-rtos/bootloader/firmware_prebuilt/blank_config.bin 0x2000 ./../../Ads/cppAds/Build/exe/adsCpp/esp8266/AdsCpp.bin


Process elf into bin with esptool.py:? (what is wrong?)
python2 ../../../../../../Esp/openSDK/esptool.py elf2image --version 2 -ff 40m -fm qio -fs 32m AdsCpp
