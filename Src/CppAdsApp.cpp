#include "CppAdsApp.hpp"
#include "OsAssert.hpp"
#include "Socket/SocketWorker.hpp"

#include <new>

namespace CppAds
{
CppAdsApp::CppAdsApp()
    : m_ProcessImage(), m_MonitorIn( "KL1408" ), m_MonitorOut( "KL2408" )
{
    for ( size_t i = 0; i < SOCKET_WORKERS_COUNT; i++ )
    {
        m_pSocketWorkers[i] = new SocketWorker();
        Osal::OsAssert( m_pSocketWorkers != NULL );
    }

    m_pInput0 =
      new KL1408Input( 0xF060U, 0x0000U, 2U, DataProviderIf::AccessType::READ );
    m_pOutput0 = new KL2408Output( 0xF060U, 0x0000U, 2U,
                                   DataProviderIf::AccessType::WRITE );
    m_pIoMemoryR = new MemoryDataProvider( 0xF060U, 0x0002U, 2U,
                                           DataProviderIf::AccessType::READ );
    // m_pIoMemoryW = new ( std::nothrow ) MemoryDataProvider(
    //   0xF060U, 0x0001U, 3, DataProviderIf::AccessType::WRITE );
}

CppAdsApp::~CppAdsApp()
{
    for ( size_t i = 0; i < SOCKET_WORKERS_COUNT; i++ )
    {
        delete m_pSocketWorkers[i];
    }
    delete m_pInput0;
    delete m_pOutput0;
}

bool CppAdsApp::Initialize()
{
    // First create a queue for comamands.
    Osal::OsAssert( Osal::OsQueueCreate( m_CommandQueue, 10,
                                         sizeof( SocketWorkerParameters ) ) ==
                    Osal::Status::OK );

    m_Listener.Initialize( &m_CommandQueue );

    for ( size_t i = 0; i < SOCKET_WORKERS_COUNT; i++ )
    {
        m_pSocketWorkers[i]->Initialize( &m_CommandQueue, &m_ProcessImage );
    }

    Osal::OsAssert( m_ProcessImage.RegisterDataProvider( m_pInput0 ) == true );
    Osal::OsAssert( m_ProcessImage.RegisterDataProvider( m_pOutput0 ) == true );
    Osal::OsAssert( m_ProcessImage.RegisterDataProvider( m_pIoMemoryR ) ==
                    true );
    // Osal::OsAssert( m_ProcessImage.RegisterDataProvider( m_pIoMemoryW ) ==
    //                 true );

    m_Stimulator.Initialize( m_pIoMemoryR );

    // m_MonitorIn.Initialize( m_pInput0 );
    // m_MonitorOut.Initialize( m_pOutput0 );

    return true;
}

void CppAdsApp::Run()
{
    for ( size_t i = 0; i < SOCKET_WORKERS_COUNT; i++ )
    {
        m_pSocketWorkers[i]->Run();
    }

    // m_Stimulator.Run();
    Osal::OsTaskDelayMs( 100 );
    // m_MonitorIn.Run();
    // Osal::OsTaskDelayMs( 100 );
    // m_MonitorOut.Run();

    m_Listener.Run();
}
}
