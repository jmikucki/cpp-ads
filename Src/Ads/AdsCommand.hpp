#include "Buffer.hpp"
#include "ProcessImage.hpp"
#include <cstdint>
#include <string>

#ifndef ADSCOMMAND_HPP
#define ADSCOMMAND_HPP
namespace CppAds
{
class AdsCommand
{
  public:
    enum class CommandId : uint16_t
    {
        INVALID       = 0x0000,
        WRITE_CONTROL = 0x0005,
        READ_WRITE    = 0x0009
    };

    enum class AdsError : uint32_t
    {
        NO_ERROR = 0x00000000
    };

    AdsCommand();
    virtual ~AdsCommand();

    virtual bool BuildFromBuffer( Buffer& packetBuffer ) = 0;
    virtual void Print()                                 = 0;
    virtual bool Process( ProcessImage* pProcessImage )  = 0;
    bool GetResponseData( Buffer& packetBuffer );

    virtual CommandId GetId() = 0;

  protected:
    static const size_t COMMAND_REQUEST_DATA_SIZE  = 1024;
    static const size_t COMMAND_RESPONSE_DATA_SIZE = 1024;

    uint8_t m_RequestData[COMMAND_REQUEST_DATA_SIZE]   = {};
    uint8_t m_ResponseData[COMMAND_RESPONSE_DATA_SIZE] = {};

    size_t m_RequestDataLength  = 0;
    size_t m_ResponseDataLength = 0;

    bool m_IsProcessed = false;
};
}

#endif // ADSCOMMAND_HPP
