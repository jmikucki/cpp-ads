#include "AdsPacket.hpp"
#include "Buffer.hpp"
#include "Endianness.hpp"
#include <cstring>

#include "ProcessImage.hpp"

#ifndef AMSPACKET_HPP
#define AMSPACKET_HPP

namespace CppAds
{
class AmsPacket
{
  public:
    AmsPacket();
    ~AmsPacket();

    bool BuildFromBuffer( Buffer& packetBuffer );
    void Print();
    bool Process( ProcessImage* pProcessImage );

    bool GetResponseData( Buffer& packetBuffer );

  private:
    /// Data fields in the packet

    // Reserved 2 bytes. Must be zero
    uint16_t m_Reserved = 0x00;

    // Length
    uint32_t m_Length = 0x00000000;

    // AdsPacket Data
    AdsPacket m_AdsPacket;

    static const size_t SIZE_OF_AMS_HEADER = 6;
};
}

#endif // AMSPACKET_HPP
