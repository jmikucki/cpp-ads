#include "AdsCommand.hpp"
#include <cstdint>
#include <cstring> //For size_t

#ifndef ADSCOMMANDREADWRITE_HPP
#define ADSCOMMANDREADWRITE_HPP

namespace CppAds
{
class AdsCommandReadWrite : public AdsCommand
{
  public:
    AdsCommandReadWrite();
    virtual ~AdsCommandReadWrite();

    virtual bool BuildFromBuffer( Buffer& packetBuffer );
    virtual void Print();
    virtual bool Process( ProcessImage* pProcessImage );
    virtual AdsCommand::CommandId GetId();

  private:
    static const AdsCommand::CommandId m_CommandId =
      AdsCommand::CommandId::READ_WRITE;
    static const size_t SIZE_OF_COMMAND_HEADER_READWRITE = 16U;

    // Command data in the packet
    uint32_t m_IndexGroup  = 0;
    uint32_t m_IndexOffset = 0;

    uint32_t m_ReadLength  = 0;
    uint32_t m_WriteLength = 0;
};
}

#endif // ADSCOMMANDREADWRITE_HPP
