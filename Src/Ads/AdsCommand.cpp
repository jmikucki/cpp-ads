#include "AdsCommand.hpp"
#include <algorithm>
#include <cstring>
//#include <iostream>

namespace CppAds
{

AdsCommand::AdsCommand()
{
}

AdsCommand::~AdsCommand()
{
}

bool AdsCommand::GetResponseData( Buffer& packetBuffer )
{
    size_t size = AdsCommand::COMMAND_REQUEST_DATA_SIZE;
    if ( m_IsProcessed )
    {
        std::memcpy( packetBuffer.GetBuffer() + packetBuffer.GetCurrentOffset(),
                     m_ResponseData, m_ResponseDataLength );
        packetBuffer.SetCurrentOffset( packetBuffer.GetCurrentOffset() +
                                       m_ResponseDataLength );

        // std::cout << "Response data lenght: " << m_ResponseDataLength
        //           << std::endl;
        return true;
    }
    else
    {
        return false;
    }
}
}
