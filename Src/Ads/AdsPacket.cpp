#include "AdsPacket.hpp"
#include "AdsCommand.hpp"
#include "AdsCommandFactory.hpp"
#include "Buffer.hpp"
//#include "Endianness.hpp"
#include "OsAssert.hpp"
//#include <iostream>

namespace CppAds
{

AdsCommandFactory AdsPacket::m_CommandFactory;

AdsPacket::AdsPacket()
{
}

AdsPacket::~AdsPacket()
{
}

bool AdsPacket::BuildFromBuffer( Buffer& packetBuffer )
{
    Osal::OsAssert( packetBuffer.GetDataLengthToFull() >= SIZE_OF_ADS_HEADER );

    for ( size_t i = 0; i < SIZE_OF_NETID; ++i )
    {
        Osal::OsAssert( packetBuffer.GetU8( m_AmsNetIdTarget[i] ) ==
                        BufferStatus::SUCCESS );
    }

    packetBuffer.GetU16( m_AmsPortTarget );

    for ( size_t i = 0; i < SIZE_OF_NETID; ++i )
    {
        Osal::OsAssert( packetBuffer.GetU8( m_AmsNetIdSource[i] ) ==
                        BufferStatus::SUCCESS );
    }

    Osal::OsAssert( packetBuffer.GetU16( m_AmsPortSource ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.GetU16( m_CommandId ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.GetU16( m_StateFlags ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.GetU32( m_DataLength ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.GetU32( m_ErrorCode ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.GetU32( m_InvokeId ) ==
                    BufferStatus::SUCCESS );

    m_pAdsCommand = m_CommandFactory.GetAdsCommandById( m_CommandId );
    if ( m_pAdsCommand == NULL )
    {
        Osal::OsAssert( m_pAdsCommand != nullptr );
        return false;
    }

    Osal::OsAssert( m_DataLength == packetBuffer.GetDataLengthToFull() );
    m_pAdsCommand->BuildFromBuffer( packetBuffer );

    return true;
}

void AdsPacket::Print()
{
    // std::cout << std::dec;
    //
    // std::cout << "m_AmsNetIdTarget: " << static_cast<int>(
    // m_AmsNetIdTarget[0] )
    //           << "." << static_cast<int>( m_AmsNetIdTarget[1] ) << "."
    //           << static_cast<int>( m_AmsNetIdTarget[2] ) << "."
    //           << static_cast<int>( m_AmsNetIdTarget[3] ) << "."
    //           << static_cast<int>( m_AmsNetIdTarget[4] ) << "."
    //           << static_cast<int>( m_AmsNetIdTarget[5] );
    // std::cout << std::endl;
    //
    // std::cout << "m_AmsPortTarget: " << m_AmsPortTarget;
    // std::cout << std::endl;
    //
    // std::cout << "m_AmsNetIdSource: " << std::dec
    //           << static_cast<int>( m_AmsNetIdSource[0] ) << "."
    //           << static_cast<int>( m_AmsNetIdSource[1] ) << "."
    //           << static_cast<int>( m_AmsNetIdSource[2] ) << "."
    //           << static_cast<int>( m_AmsNetIdSource[3] ) << "."
    //           << static_cast<int>( m_AmsNetIdSource[4] ) << "."
    //           << static_cast<int>( m_AmsNetIdSource[5] );
    // std::cout << std::endl;
    //
    // std::cout << "m_AmsPortSource: " << m_AmsPortSource;
    // std::cout << std::endl;
    //
    // std::cout << "m_CommandId: " << std::dec << m_CommandId << std::endl;
    // std::cout << "m_StateFlags: " << std::showbase << std::hex <<
    // m_StateFlags
    //           << std::endl;
    // std::cout << "m_DataLength: " << std::dec << m_DataLength << std::endl;
    // std::cout << "m_ErrorCode: " << std::dec << m_ErrorCode << std::endl;
    // std::cout << "m_InvokeId: " << std::showbase << std::hex << m_InvokeId
    //           << std::endl;

    m_pAdsCommand->Print();
}

bool AdsPacket::Process( ProcessImage* pProcessImage )
{
    Osal::OsAssert( m_pAdsCommand != NULL );

    return m_pAdsCommand->Process( pProcessImage );
}

bool AdsPacket::GetResponseData( Buffer& packetBuffer )
{
    Osal::OsAssert( packetBuffer.GetBufferLenghtToFull() >=
                    SIZE_OF_ADS_HEADER );

    // Returend packet should have target/source opposite.
    for ( size_t i = 0; i < SIZE_OF_NETID; ++i )
    {
        Osal::OsAssert( packetBuffer.PutU8( m_AmsNetIdSource[i] ) ==
                        BufferStatus::SUCCESS );
    }

    Osal::OsAssert( packetBuffer.PutU16( m_AmsPortSource ) ==
                    BufferStatus::SUCCESS );

    for ( size_t i = 0; i < SIZE_OF_NETID; ++i )
    {
        Osal::OsAssert( packetBuffer.PutU8( m_AmsNetIdTarget[i] ) ==
                        BufferStatus::SUCCESS );
    }

    Osal::OsAssert( packetBuffer.PutU16( m_AmsPortTarget ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.PutU16( m_CommandId ) ==
                    BufferStatus::SUCCESS );

    // State flag shall indicate that it's a response.
    m_StateFlags |= 0x1U;
    Osal::OsAssert( packetBuffer.PutU16( m_StateFlags ) ==
                    BufferStatus::SUCCESS );

    // We don't know the length yet. Just remember the offset and store dummy
    // byte.
    size_t offset = packetBuffer.GetCurrentOffset();
    Osal::OsAssert( packetBuffer.PutU32( 0xDEADBEEF ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.PutU32( m_ErrorCode ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.PutU32( m_InvokeId ) ==
                    BufferStatus::SUCCESS );

    uint32_t adsDataLength =
      static_cast<uint32_t>( packetBuffer.GetDataLength() );

    Osal::OsAssert( m_pAdsCommand->GetResponseData( packetBuffer ) == true );

    uint32_t commandDataLength =
      static_cast<uint32_t>( packetBuffer.GetDataLength() ) - adsDataLength;

    size_t endOffset = packetBuffer.GetCurrentOffset();

    // Set the proper length.
    Osal::OsAssert( packetBuffer.SetCurrentOffset( offset ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.PutU32( commandDataLength ) ==
                    BufferStatus::SUCCESS );

    // Revert offset to the end of packet.
    packetBuffer.SetCurrentOffset( endOffset );

    return true;
}
}
