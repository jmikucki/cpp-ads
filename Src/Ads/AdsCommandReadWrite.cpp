#include "AdsCommandReadWrite.hpp"
#include "Endianness.hpp"
#include "OsAssert.hpp"
#include <algorithm>
#include <cstring>
//#include <iostream>

namespace CppAds
{
AdsCommandReadWrite::AdsCommandReadWrite()
{
}

AdsCommandReadWrite::~AdsCommandReadWrite()
{
}

AdsCommand::CommandId AdsCommandReadWrite::GetId()
{
    return m_CommandId;
}

bool AdsCommandReadWrite::BuildFromBuffer( Buffer& packetBuffer )
{
    Osal::OsAssert( packetBuffer.GetDataLengthToFull() >=
                    SIZE_OF_COMMAND_HEADER_READWRITE );

    Osal::OsAssert( packetBuffer.GetU32( m_IndexGroup ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.GetU32( m_IndexOffset ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.GetU32( m_ReadLength ) ==
                    BufferStatus::SUCCESS );

    Osal::OsAssert( packetBuffer.GetU32( m_WriteLength ) ==
                    BufferStatus::SUCCESS );

    // Make sure that lenght of read data is correct and that
    // we can fit it in our buffer.
    Osal::OsAssert( packetBuffer.GetDataLengthToFull() == m_WriteLength );
    Osal::OsAssert( m_WriteLength <= COMMAND_REQUEST_DATA_SIZE );

    // Copy command data from the packet.
    std::memcpy( m_RequestData,
                 packetBuffer.GetBuffer() + packetBuffer.GetCurrentOffset(),
                 m_WriteLength );

    m_RequestDataLength = m_WriteLength;

    return true;
}

bool AdsCommandReadWrite::Process( ProcessImage* pProcessImage )
{

    for ( size_t i = 0; i < m_WriteLength; ++i )
    {
        bool result = pProcessImage->WriteByte( m_IndexGroup, m_IndexOffset + i,
                                                m_RequestData[i] );
        if ( result != true )
        {
            return false;
        }
    }

    uint32_t errorCode = 0x0000;

    *reinterpret_cast<uint32_t*>( &m_ResponseData[0] ) =
      HostNetworku32( errorCode );
    *reinterpret_cast<uint32_t*>( &m_ResponseData[4] ) =
      HostNetworku32( m_ReadLength );

    for ( size_t i = 0; i < m_ReadLength; ++i )
    {
        bool result = pProcessImage->ReadByte( m_IndexGroup, m_IndexOffset + i,
                                               m_ResponseData[8 + i] );
        if ( result != true )
        {
            return false;
        }
    }
    m_IsProcessed        = true;
    m_ResponseDataLength = m_ReadLength + 8;

    return true;
}

void AdsCommandReadWrite::Print()
{
    // std::cout << std::endl;
    // std::cout << "Command: ReadWrite" << std::endl;
    // std::cout << "IndexGroup: " << std::hex << m_IndexGroup << std::endl;
    // std::cout << "IndexOffset: " << std::hex << m_IndexOffset << std::endl;
    // std::cout << "Read length: " << std::hex << m_ReadLength << std::endl;
    // std::cout << "Write length: " << std::hex << m_WriteLength << std::endl;
}
}
