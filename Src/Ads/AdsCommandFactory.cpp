#include "AdsCommandFactory.hpp"
#include "OsAssert.hpp"
//#include <iostream>

namespace CppAds
{

AdsCommandFactory::AdsCommandFactory()
{
    // Register commands
    // std::cout << "Id of rw: "
    //           << static_cast<uint16_t>( m_CommandReadWrite.GetId() )
    //           << std::endl;
    Osal::OsAssert(
      m_CommandRegistry[static_cast<uint16_t>( m_CommandReadWrite.GetId() )] ==
      NULL );
    m_CommandRegistry[static_cast<uint16_t>( m_CommandReadWrite.GetId() )] =
      &m_CommandReadWrite;

    // std::cout << "Id of wc: "
    //           << static_cast<uint16_t>( m_CommandWriteControl.GetId() )
    //           << std::endl;
    Osal::OsAssert( m_CommandRegistry[static_cast<uint16_t>(
                      m_CommandWriteControl.GetId() )] == NULL );
    m_CommandRegistry[static_cast<uint16_t>( m_CommandWriteControl.GetId() )] =
      &m_CommandWriteControl;
}

AdsCommandFactory::~AdsCommandFactory()
{
}

AdsCommand* AdsCommandFactory::GetAdsCommandById( uint16_t commandId )
{
    Osal::OsAssert(
      commandId == static_cast<uint16_t>( m_CommandReadWrite.GetId() ) ||
      commandId == static_cast<uint16_t>( m_CommandWriteControl.GetId() ) );
    return m_CommandRegistry[commandId];
}
}
