#include "AmsPacket.hpp"
#include "Buffer.hpp"
#include "OsAssert.hpp"
//#include <iostream>

namespace CppAds
{

AmsPacket::AmsPacket()
{
}

AmsPacket::~AmsPacket()
{
}

bool AmsPacket::BuildFromBuffer( Buffer& packetBuffer )
{
    Osal::OsAssert( packetBuffer.GetDataLengthToFull() >= SIZE_OF_AMS_HEADER );

    Osal::OsAssert( packetBuffer.GetU16( m_Reserved ) ==
                    BufferStatus::SUCCESS );
    if ( m_Reserved != 0x0000U )
    {
        return false;
    }

    Osal::OsAssert( packetBuffer.GetU32( m_Length ) == BufferStatus::SUCCESS );
    // uint32_t len = HostNetworku32( pBuffer + sizeof( m_Reserved ) );
    // m_Length     = HostNetworku32( len );

    Osal::OsAssert( packetBuffer.GetDataLengthToFull() == m_Length );

    m_AdsPacket.BuildFromBuffer( packetBuffer );

    return true;
}

void AmsPacket::Print()
{
    // std::cout << "Reserved: " << std::showbase << std::hex << m_Reserved
    //           << std::endl;
    // std::cout << "Length: " << std::dec << m_Length << std::endl;

    m_AdsPacket.Print();
}

bool AmsPacket::Process( ProcessImage* pProcessImage )
{
    return m_AdsPacket.Process( pProcessImage );
}

bool AmsPacket::GetResponseData( Buffer& packetBuffer )
{
    Osal::OsAssert( packetBuffer.GetBufferLenghtToFull() >=
                    SIZE_OF_AMS_HEADER );

    // Reserved should always be zero.
    packetBuffer.PutU16( m_Reserved );

    // We don't know length yet, let's wait for that information.
    size_t lengthOffset = packetBuffer.GetCurrentOffset();
    packetBuffer.PutU32( 0 );

    uint32_t amsDataLength =
      static_cast<uint32_t>( packetBuffer.GetDataLength() );

    bool result = m_AdsPacket.GetResponseData( packetBuffer );

    uint32_t adsDataLength =
      static_cast<uint32_t>( packetBuffer.GetDataLength() ) - amsDataLength;

    // Now that we know size, store it.
    packetBuffer.SetCurrentOffset( lengthOffset );
    packetBuffer.PutU32( adsDataLength );

    return result;
}
}
