#include "AdsCommand.hpp"

#ifndef ADSCOMMANDWRITECONTROL_HPP
#define ADSCOMMANDWRITECONTROL_HPP

namespace CppAds
{
class AdsCommandWriteControl : public AdsCommand
{
  public:
    AdsCommandWriteControl();
    virtual ~AdsCommandWriteControl();

    virtual bool BuildFromBuffer( Buffer& packetBuffer );
    virtual void Print();
    virtual bool Process( ProcessImage* pProcessImage );
    virtual AdsCommand::CommandId GetId();

  private:
    static const AdsCommand::CommandId m_CommandId =
      AdsCommand::CommandId::WRITE_CONTROL;
    static const size_t SIZE_OF_COMMAND_HEADER_WRITECONTROL = 8U;

    bool m_IsPingOnly = false;

    // Command data in the packet
    uint16_t m_AdsState    = 0;
    uint16_t m_DeviceState = 0;
    uint32_t m_DataLength  = 0;
};
}

#endif // ADSCOMMANDWRITECONTROL_HPP
