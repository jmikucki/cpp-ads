
#include <cstdint>
#include <cstring>

#ifndef DATALINKLAYERREQUEST_HPP
#define DATALINKLAYERREQUEST_HPP

namespace eBus
{
namespace DataLinkLayer
{
    class DataLinkLayerRequest
    {
      public:
        uint8_t* GetBuffer()
        {
            return m_pBuffer;
        }
        size_t GetSize()
        {
            return m_Size;
        }

      private:
        uint8_t* m_pBuffer = nullptr;
        size_t m_Size      = 0;
    };
}
}

#endif // DATALINKLAYERREQUEST_HPP
