

#include "DataLinkLayerRequest.hpp"

#ifndef DATALINKLAYERLISTENER_HPP
#define DATALINKLAYERLISTENER_HPP

namespace eBus
{
namespace DataLinkLayer
{
    class DataLinkLayerListener
    {
      public:
        DataLinkLayerListener()  = default;
        ~DataLinkLayerListener() = default;

        DataLinkLayerListener( const DataLinkLayerListener& other )  = delete;
        DataLinkLayerListener( const DataLinkLayerListener&& other ) = delete;
        DataLinkLayerListener&
        operator=( const DataLinkLayerListener& other ) = delete;
        DataLinkLayerListener&
        operator=( DataLinkLayerListener&& other ) = delete;

        virtual void NotifyNewRequest( DataLinkLayerRequest& rRequest ) = 0;

      private:
    };
}
}

#endif // DATALINKLAYERLISTENER_HPP
