
#include "DataLinkLayerListener.hpp"
#include "OsInterprocess.hpp"
#include <cstdint>

#ifndef TRANSMITTER_HPP
#define TRANSMITTER_HPP

namespace eBus
{
namespace LinkLayer
{
    class Transmitter
    {
      private:
        // Listener to notify when new data is received.
        eBus::DataLinkLayer::DataLinkLayerListener* pDataLinkLayerListener =
          nullptr;

        // Data buffer for received data.
        uint8_t* pReceiveBuffer = nullptr;

        // Queue used to pass received packets up the stack
        Osal::OsQueueType m_ReceiveQueue;

        // Queue used to get packets to transmit from the stack
        Osal::OsQueueType m_TransmitQueue;
    };
}
}

#endif // TRANSMITTER_HPP
