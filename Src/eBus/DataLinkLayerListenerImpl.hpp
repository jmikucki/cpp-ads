
#include "DataLinkLayerListener.hpp"
#include "DataLinkLayerRequest.hpp"

#ifndef DATALINKLAYERLISTENERIMPL_HPP
#define DATALINKLAYERLISTENERIMPL_HPP

namespace eBus
{
namespace DataLinkLayer
{
    class DataLinkLayerListenerImpl : public DataLinkLayerListener
    {
      public:
        DataLinkLayerListenerImpl()  = default;
        ~DataLinkLayerListenerImpl() = default;

        DataLinkLayerListenerImpl( const DataLinkLayerListenerImpl& other ) =
          delete;
        DataLinkLayerListenerImpl( const DataLinkLayerListenerImpl&& other ) =
          delete;
        DataLinkLayerListenerImpl&
        operator=( const DataLinkLayerListenerImpl& other ) = delete;
        DataLinkLayerListenerImpl&
        operator=( DataLinkLayerListenerImpl&& other ) = delete;

        void NotifyNewRequest( DataLinkLayerRequest& rRequest );

      private:
    };
}
}

#endif // DATALINKLAYERLISTENERIMPL_HPP
