#include "ProcessImage.hpp"
#include "OsAssert.hpp"

namespace CppAds
{
ProcessImage::ProcessImage() : m_FirstEmptyRead( 0 ), m_FirstEmptyWrite( 0 )
{
    Osal::OsAssert( Osal::OsMutexCreate( m_Lock ) == Osal::Status::OK );
}

ProcessImage::~ProcessImage()
{
    Osal::OsMutexDelete( m_Lock );
}

bool ProcessImage::RegisterDataProvider( DataProviderIf* pProvider )
{

    for ( size_t i = 0; i < pProvider->GetOffsetSize(); i++ )
    {

        DataProviderIf* pRead =
          GetProviderByIndex( pProvider->GetGroup(), pProvider->GetOffset() + i,
                              DataProviderIf::AccessType::READ );

        DataProviderIf* pWrite =
          GetProviderByIndex( pProvider->GetGroup(), pProvider->GetOffset() + i,
                              DataProviderIf::AccessType::WRITE );

        if ( pRead != NULL &&
             ( pProvider->GetAccessType() == DataProviderIf::AccessType::READ ||
               pProvider->GetAccessType() ==
                 DataProviderIf::AccessType::READWRITE ) )
        {
            return false;
        }

        if ( pWrite != NULL && ( pProvider->GetAccessType() ==
                                   DataProviderIf::AccessType::WRITE ||
                                 pProvider->GetAccessType() ==
                                   DataProviderIf::AccessType::READWRITE ) )
        {
            return false;
        }
    }

    // Didn't find conflicts
    if ( pProvider->GetAccessType() == DataProviderIf::AccessType::WRITE ||
         pProvider->GetAccessType() == DataProviderIf::AccessType::READWRITE )
    {
        m_ProcessDataProvidersWrite[m_FirstEmptyWrite++] = pProvider;
    }

    if ( pProvider->GetAccessType() == DataProviderIf::AccessType::READ ||
         pProvider->GetAccessType() == DataProviderIf::AccessType::READWRITE )
    {
        m_ProcessDataProvidersRead[m_FirstEmptyRead++] = pProvider;
    }

    return true;
}
bool ProcessImage::ReadByte( uint32_t group, uint32_t offset,
                             uint8_t& dataByte )
{
    bool result = false;

    DataProviderIf* pProvider =
      GetProviderByIndex( group, offset, DataProviderIf::AccessType::READ );
    if ( pProvider != NULL )
    {
        Osal::OsMutexLock( m_Lock );
        result = pProvider->ReadByte( group, offset - pProvider->GetOffset(),
                                      dataByte );
        Osal::OsMutexUnlock( m_Lock );
    }

    return result;
}

bool ProcessImage::WriteByte( uint32_t group, uint32_t offset,
                              uint8_t dataByte )
{
    bool result = false;

    DataProviderIf* pProvider =
      GetProviderByIndex( group, offset, DataProviderIf::AccessType::WRITE );
    if ( pProvider != NULL )
    {
        Osal::OsMutexLock( m_Lock );
        result = pProvider->WriteByte( group, offset - pProvider->GetOffset(),
                                       dataByte );
        Osal::OsMutexUnlock( m_Lock );
    }

    return result;
}

DataProviderIf*
ProcessImage::GetProviderByIndex( size_t group, size_t offset,
                                  DataProviderIf::AccessType accessType )
{
    for ( size_t i = 0; i < MAX_INDEXES; ++i )
    {
        DataProviderIf* pEntryRead  = NULL;
        DataProviderIf* pEntryWrite = NULL;

        switch ( accessType )
        {
        case DataProviderIf::AccessType::READ:
            pEntryRead  = m_ProcessDataProvidersRead[i];
            pEntryWrite = NULL;
            break;
        case DataProviderIf::AccessType::WRITE:
            pEntryRead  = NULL;
            pEntryWrite = m_ProcessDataProvidersWrite[i];
            break;
        case DataProviderIf::AccessType::READWRITE:
        // Fall through.
        default:
            return NULL;
        }

        if ( pEntryRead != NULL )
        {
            if ( group == pEntryRead->GetGroup() &&
                 offset >= pEntryRead->GetOffset() &&
                 offset <
                   pEntryRead->GetOffset() + pEntryRead->GetOffsetSize() )
            {
                return pEntryRead;
            }
        }

        if ( pEntryWrite != NULL )
        {
            if ( group == pEntryWrite->GetGroup() &&
                 offset >= pEntryWrite->GetOffset() &&
                 offset <
                   pEntryWrite->GetOffset() + pEntryWrite->GetOffsetSize() )
            {
                return pEntryWrite;
            }
        }
    }
    return NULL;
}
}
