#include <cstdint>
#include <cstring> //For size_t

#ifndef DATAPROVIDERIF_HPP
#define DATAPROVIDERIF_HPP

namespace CppAds
{
class DataProviderIf
{
  public:
    enum class AccessType
    {
        READ,
        WRITE,
        READWRITE
    };

    virtual void SetOffset( const size_t offset ) = 0;
    virtual size_t GetOffset()                    = 0;
    virtual size_t GetOffsetSize()                = 0;
    virtual bool IsOffsetFixed()                  = 0;
    virtual size_t GetGroup()                     = 0;
    virtual AccessType GetAccessType()            = 0;

    virtual bool ReadByte( uint32_t group, uint32_t subOffset,
                           uint8_t& dataRead ) = 0;
    virtual bool WriteByte( uint32_t group, uint32_t subOffset,
                            uint8_t dataByte ) = 0;
};
}

#endif // DATAPROVIDERIF_HPP
