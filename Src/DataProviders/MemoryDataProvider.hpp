#include "DataProviderIf.hpp"
#include <cstdint>
#include <cstring> //For size_t

#ifndef MEMORYDATAPROVIDER_HPP
#define MEMORYDATAPROVIDER_HPP

namespace CppAds
{
class MemoryDataProvider : public DataProviderIf
{
  public:
    static const size_t MAX_LENGTH_OF_INDEX_DATA = 1024;

    MemoryDataProvider( const size_t group, const size_t offset,
                        const size_t offsetSize,
                        const DataProviderIf::AccessType accessType );
    virtual ~MemoryDataProvider();

    size_t GetGroup();
    size_t GetGroupSize();

    void SetOffset( const size_t offset );
    size_t GetOffset();
    size_t GetOffsetSize();
    bool IsOffsetFixed();

    DataProviderIf::AccessType GetAccessType();
    bool ReadByte( uint32_t group, uint32_t offset, uint8_t& dataRead );
    bool WriteByte( uint32_t group, uint32_t offset, uint8_t dataByte );

  private:
    size_t m_Group;

    size_t m_Offset;
    size_t m_OffsetSize;

    DataProviderIf::AccessType m_AccessType;

    uint8_t m_DataArray[MAX_LENGTH_OF_INDEX_DATA] = {};
};
}

#endif // MEMORYDATAPROVIDER_HPP
