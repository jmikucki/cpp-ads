#include "OsTask.hpp"
#include "ProcessImage.hpp"

#ifndef PROCESSIMAGESTIMULATOR_HPP
#define PROCESSIMAGESTIMULATOR_HPP

namespace CppAds
{
class ProcessImageStimulator
{
  public:
    ProcessImageStimulator();
    ~ProcessImageStimulator();

    void Initialize( DataProviderIf* pProvider );
    void Run();
    static void RunThreadFunction( void* pParameter );

    void Stimulate();

  private:
    ProcessImageStimulator( const ProcessImageStimulator& other ) = delete;
    ProcessImageStimulator( ProcessImageStimulator&& other )      = delete;
    ProcessImageStimulator&
    operator=( const ProcessImageStimulator& other ) = delete;
    ProcessImageStimulator&
    operator=( ProcessImageStimulator&& other ) = delete;

    DataProviderIf* m_pProvider;
    Osal::OsTaskType m_Task;
};
}

#endif // PROCESSIMAGESTIMULATOR_HPP
