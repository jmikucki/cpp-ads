#include <cstdint>
#include <cstring>

#ifndef BUFFER_HPP
#define BUFFER_HPP

enum class BufferStatus : uint8_t
{
    SUCCESS,
    ERROR,
    OUTOFBOUNDS
};

class Buffer
{
  public:
    Buffer( uint8_t* const pPreallocatedMemory, size_t maxLength )
    {
        m_pMemoryBuffer         = pPreallocatedMemory;
        m_MemoryWasPreallocated = true;
        m_BufferLenght          = maxLength;
        m_OffsetCurrent         = 0;
        m_OffsetLast            = 0;
    }

    Buffer( size_t maxLength )
    {
        m_pMemoryBuffer = new uint8_t[maxLength];

        if ( m_pMemoryBuffer != nullptr )
        {
            m_MemoryWasPreallocated = false;
            m_BufferLenght          = maxLength;
            m_OffsetCurrent         = 0;
            m_OffsetLast            = 0;
        }
    }

    ~Buffer()
    {
        if ( m_pMemoryBuffer != nullptr )
        {
            if ( m_MemoryWasPreallocated == false )
            {
                delete[] m_pMemoryBuffer;
            }

            m_BufferLenght = 0;
        }
    }

    BufferStatus PutU8( const uint8_t dataIn )
    {
        if ( m_OffsetCurrent + 1 > m_BufferLenght )
        {
            return BufferStatus::OUTOFBOUNDS;
        }

        m_pMemoryBuffer[m_OffsetCurrent++] = ( dataIn >> 0 ) & 0xFF;

        if ( m_OffsetLast < m_OffsetCurrent )
        {
            m_OffsetLast = m_OffsetCurrent;
        }

        return BufferStatus::SUCCESS;
    }

    BufferStatus GetU8( uint8_t& dataOut )
    {
        if ( m_OffsetCurrent + 1 > m_OffsetLast )
        {
            return BufferStatus::OUTOFBOUNDS;
        }

        dataOut = m_pMemoryBuffer[m_OffsetCurrent++];

        return BufferStatus::SUCCESS;
    }

    BufferStatus PutU16( const uint16_t dataIn )
    {
        if ( m_OffsetCurrent + 2 > m_BufferLenght )
        {
            return BufferStatus::OUTOFBOUNDS;
        }

        m_pMemoryBuffer[m_OffsetCurrent++] = ( dataIn >> 0 ) & 0xFF;
        m_pMemoryBuffer[m_OffsetCurrent++] = ( dataIn >> 8 ) & 0xFF;

        if ( m_OffsetLast < m_OffsetCurrent )
        {
            m_OffsetLast = m_OffsetCurrent;
        }

        return BufferStatus::SUCCESS;
    }

    BufferStatus GetU16( uint16_t& dataOut )
    {
        if ( m_OffsetCurrent + 2 > m_OffsetLast )
        {
            return BufferStatus::OUTOFBOUNDS;
        }

        dataOut = ( ( m_pMemoryBuffer[m_OffsetCurrent] ) << 0 ) +
                  ( ( m_pMemoryBuffer[m_OffsetCurrent + 1] ) << 8 );

        m_OffsetCurrent += 2;

        return BufferStatus::SUCCESS;
    }

    BufferStatus PutU32( const uint32_t dataIn )
    {
        if ( m_OffsetCurrent + 4 > m_BufferLenght )
        {
            return BufferStatus::OUTOFBOUNDS;
        }

        m_pMemoryBuffer[m_OffsetCurrent++] = ( dataIn >> 0 ) & 0xFF;
        m_pMemoryBuffer[m_OffsetCurrent++] = ( dataIn >> 8 ) & 0xFF;
        m_pMemoryBuffer[m_OffsetCurrent++] = ( dataIn >> 16 ) & 0xFF;
        m_pMemoryBuffer[m_OffsetCurrent++] = ( dataIn >> 24 ) & 0xFF;

        if ( m_OffsetLast < m_OffsetCurrent )
        {
            m_OffsetLast = m_OffsetCurrent;
        }

        return BufferStatus::SUCCESS;
    }

    BufferStatus GetU32( uint32_t& dataOut )
    {
        if ( m_OffsetCurrent + 4 > m_OffsetLast )
        {
            return BufferStatus::OUTOFBOUNDS;
        }

        dataOut = ( ( m_pMemoryBuffer[m_OffsetCurrent] ) << 0 ) +
                  ( ( m_pMemoryBuffer[m_OffsetCurrent + 1] ) << 8 ) +
                  ( ( m_pMemoryBuffer[m_OffsetCurrent + 2] ) << 16 ) +
                  ( ( m_pMemoryBuffer[m_OffsetCurrent + 3] ) << 24 );

        m_OffsetCurrent += 4;

        return BufferStatus::SUCCESS;
    }

    size_t GetCurrentOffset()
    {
        return m_OffsetCurrent;
    }

    BufferStatus SetCurrentOffset( const size_t offset )
    {

        if ( offset > m_BufferLenght )
        {
            return BufferStatus::OUTOFBOUNDS;
        }

        m_OffsetCurrent = offset;

        if ( m_OffsetLast < m_OffsetCurrent )
        {
            m_OffsetLast = m_OffsetCurrent;
        }
        return BufferStatus::SUCCESS;
    }

    uint8_t* GetBuffer()
    {
        return m_pMemoryBuffer;
    }

    size_t GetDataLength()
    {
        return m_OffsetLast;
    }

    BufferStatus SetDataLength( size_t dataLenght )
    {
        BufferStatus status = BufferStatus::SUCCESS;

        if ( dataLenght <= m_BufferLenght )
        {
            m_OffsetLast = dataLenght;
        }
        else
        {
            status = BufferStatus::OUTOFBOUNDS;
        }

        return status;
    }

    size_t GetDataLengthToFull()
    {
        return m_OffsetLast - m_OffsetCurrent;
    }

    size_t GetBufferLenghtToFull()
    {
        return m_BufferLenght - m_OffsetLast;
    }

    void Reset()
    {
        m_OffsetLast    = 0;
        m_OffsetCurrent = 0;
    }

  private:
    Buffer( const Buffer& other ) = delete;
    Buffer( Buffer&& other )      = delete;
    Buffer& operator=( const Buffer& other ) = delete;
    Buffer& operator=( Buffer&& other ) = delete;

    uint8_t* m_pMemoryBuffer;

    size_t m_BufferLenght;
    size_t m_OffsetLast;
    size_t m_OffsetCurrent;

    bool m_MemoryWasPreallocated;
};

#endif // BUFFER_HPP
