#include "ProcessImageStimulator.hpp"
#include "DataProviderIf.hpp"
#include "OsAssert.hpp"
#include "OsTask.hpp"
//#include <iostream>

namespace CppAds
{

ProcessImageStimulator::ProcessImageStimulator()
{
}
ProcessImageStimulator::~ProcessImageStimulator()
{
}

void ProcessImageStimulator::Initialize( DataProviderIf* pProvider )
{
    m_pProvider = pProvider;
}

void ProcessImageStimulator::Run()
{
    Osal::OsTaskCreate( ProcessImageStimulator::RunThreadFunction,
                        reinterpret_cast<void*>( this ), 0x4000, 1,
                        "ProcessImageStimulator", m_Task );
}

void ProcessImageStimulator::RunThreadFunction( void* pParameter )
{
    ProcessImageStimulator* pInstance =
      reinterpret_cast<ProcessImageStimulator*>( pParameter );

    pInstance->Stimulate();
}

void ProcessImageStimulator::Stimulate()
{
    while ( true )
    {
        uint8_t dataByte;

        Osal::OsAssert( m_pProvider->ReadByte( 0xF060, 0, dataByte ) == true );
        // std::cout << "Read: " << std::hex << (int)dataByte << std::endl;

        if ( dataByte == 0 )
        {
            dataByte = 1;
        }
        else
        {
            dataByte = 0; // dataByte << 1;
        }

        // std::cout << "Write: " << std::hex << (int)dataByte << std::endl;
        Osal::OsAssert( m_pProvider->WriteByte( 0xF060, 0, dataByte ) == true );

        Osal::OsTaskDelayMs( 1000 );
    }
}
}
