#include "ProcessImageMonitor.hpp"
#include "DataProviderIf.hpp"
#include "OsAssert.hpp"
#include "OsTask.hpp"
//#include <iostream>

namespace CppAds
{

ProcessImageMonitor::ProcessImageMonitor( const char* const pName )
    : m_pName( pName )
{
}
ProcessImageMonitor::~ProcessImageMonitor()
{
}

void ProcessImageMonitor::Initialize( DataProviderIf* pProvider )
{
    m_pProvider = pProvider;
}

void ProcessImageMonitor::Run()
{
    Osal::OsTaskCreate( ProcessImageMonitor::RunThreadFunction,
                        reinterpret_cast<void*>( this ), 0xFFFF, 1,
                        "ProcessImageMonitor", m_Task );
}

void ProcessImageMonitor::RunThreadFunction( void* pParameter )
{
    ProcessImageMonitor* pInstance =
      reinterpret_cast<ProcessImageMonitor*>( pParameter );

    pInstance->Monitor();
}

void ProcessImageMonitor::Monitor()
{
    while ( true )
    {
        uint8_t dataByte;

        Osal::OsAssert( m_pProvider->ReadByte( 0xF060, 0, dataByte ) == true );
        // std::cout << m_pName << ": " << std::hex << (int)dataByte <<
        // std::endl;

        Osal::OsTaskDelayMs( 1000 );
    }
}
}
