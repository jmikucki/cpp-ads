#include "OsTask.hpp"
#include "ProcessImage.hpp"

#ifndef PROCESSIMAGEMONITOR_HPP
#define PROCESSIMAGEMONITOR_HPP

namespace CppAds
{
class ProcessImageMonitor
{
  public:
    ProcessImageMonitor( const char* const pName );
    ~ProcessImageMonitor();

    void Initialize( DataProviderIf* pProvider );
    void Run();
    static void RunThreadFunction( void* pParameter );

    void Monitor();

  private:
    ProcessImageMonitor( const ProcessImageMonitor& other ) = delete;
    ProcessImageMonitor( ProcessImageMonitor&& other )      = delete;
    ProcessImageMonitor& operator=( const ProcessImageMonitor& other ) = delete;
    ProcessImageMonitor& operator=( ProcessImageMonitor&& other ) = delete;

    DataProviderIf* m_pProvider;
    Osal::OsTaskType m_Task;
    const char* const m_pName;
};
}

#endif // PROCESSIMAGEMONITOR_HPP
