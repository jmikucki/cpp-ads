#ifndef SOCKETWORKER_HPP
#define SOCKETWORKER_HPP

#include "OsSocket.hpp"
#include "OsTask.hpp"
#include "ProcessImage.hpp"

#include <cstdint>

namespace CppAds
{

class SocketWorker
{
  public:
    SocketWorker();
    ~SocketWorker();

    void Initialize( Osal::OsQueueType* pCommandQueue,
                     ProcessImage* pProcessImage );

    void Run();
    void ServiceAllCommands();
    void ServiceSingleConnection( Osal::OsSocketType* pSocket );
    static void RunThreadFunction( void* pParameters );

  private:
    static const size_t RECEIVE_SIZE = 512;

    Osal::OsTaskType m_Task;
    Osal::OsQueueType* m_pCommandQueue;
    ProcessImage* m_pProcessImage;
};
}

#endif // SOCKETWORKER_HPP
