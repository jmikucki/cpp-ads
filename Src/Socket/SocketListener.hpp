#ifndef SOCKETLISTENER_HPP
#define SOCKETLISTENER_HPP

#include "OsInterprocess.hpp"
#include "OsSocket.hpp"
#include "ProcessImage.hpp"
#include "SocketWorker.hpp"
#include <cstdint>

#include "ProcessImage.hpp"

namespace CppAds
{

class SocketListener
{
  public:
    SocketListener();
    ~SocketListener();

    void Initialize( Osal::OsQueueType* pCommandQueue );

    void Run();

  private:
    Osal::OsSocketType m_SocketListen;
    Osal::OsQueueType* m_pCommandQueue;
};
}

#endif // SOCKETLISTENER_HPP
