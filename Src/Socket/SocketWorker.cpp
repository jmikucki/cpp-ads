#include "SocketWorker.hpp"
#include "AdsCommandFactory.hpp"
#include "AmsPacket.hpp"
#include "Buffer.hpp"
//#include "Logger.hpp"

#include "OsAssert.hpp"
#include "OsInterprocess.hpp"
#include "OsTask.hpp"

#include "SocketWorkerParameters.hpp"

//#include <iostream>

namespace CppAds
{

SocketWorker::SocketWorker()
{
}

SocketWorker::~SocketWorker()
{
}

void SocketWorker::Initialize( Osal::OsQueueType* pCommandQueue,
                               ProcessImage* pProcessImage )
{
    m_pCommandQueue = pCommandQueue;
    m_pProcessImage = pProcessImage;
}

void SocketWorker::ServiceAllCommands()
{
    Osal::Status ret = Osal::Status::OK;

    // Get command from the queue.
    SocketWorkerParameters parameters;

    while ( true )
    {
        ret = Osal::OsQueueReceiveBlocking(
          *m_pCommandQueue, reinterpret_cast<void*>( &parameters ) );

        if ( ret != Osal::Status::OK )
        {
            return;
        }
        if ( parameters.command ==
             SocketWorkerParameters::SocketWorkerCommand::FINISH )
        {
            return;
        }
        else if ( parameters.command ==
                  SocketWorkerParameters::SocketWorkerCommand::PROCESS )
        {

            ServiceSingleConnection( &parameters.socket );
        }
    }
}

void SocketWorker::ServiceSingleConnection( Osal::OsSocketType* pSocket )
{
    Osal::Status status             = Osal::Status::OK;
    Osal::SocketStatus socketStatus = Osal::SocketStatus::OK;

    Osal::OsAssert( pSocket != NULL );
    Osal::OsAssert( m_pProcessImage != NULL );

    while ( status == Osal::Status::OK &&
            socketStatus == Osal::SocketStatus::OK )
    {
        size_t dataReceivedLength                       = RECEIVE_SIZE;
        uint8_t communicationBufferMemory[RECEIVE_SIZE] = {};

        Buffer packetBuffer( communicationBufferMemory, RECEIVE_SIZE );

        // std::cout << "Trying to read some data." << std::endl;

        Osal::SocketStatus socketStatus = Osal::OsSocketReceive(
          *pSocket, packetBuffer.GetBuffer(), dataReceivedLength );

        packetBuffer.SetDataLength( dataReceivedLength );

        if ( socketStatus == Osal::SocketStatus::OK )
        {

            // std::cout << "Read " << std::dec << dataReceivedLength << "
            // bytes." << std::endl;
            // Logger::GetInstance()->LogBuffer( &receivingBuffer[0],
            // dataReceivedLength );

            AmsPacket request;
            request.BuildFromBuffer( packetBuffer );
            request.Print();

            bool result = request.Process( m_pProcessImage );
            Osal::OsAssert( result == true );

            // std::cout << "Trying to get response data." << std::endl;
            packetBuffer.Reset();
            result = request.GetResponseData( packetBuffer );

            Osal::OsAssert( result == true );

            size_t dataToSendLength = packetBuffer.GetDataLength();
            socketStatus = OsSocketSend( *pSocket, packetBuffer.GetBuffer(),
                                         dataToSendLength );

            Osal::OsAssert( socketStatus == Osal::SocketStatus::OK );
        }
        else
        {
            // std::cout << "Read failed. Closing." << std::endl;
            Osal::OsSocketClose( *pSocket );
            return;
        }
    }
}
void SocketWorker::Run()
{
    Osal::OsTaskCreate( SocketWorker::RunThreadFunction,
                        reinterpret_cast<void*>( this ), 0x2000, 13,
                        "SocketWorker", m_Task );
}

void SocketWorker::RunThreadFunction( void* pParameter )
{
    SocketWorker* pInstance = reinterpret_cast<SocketWorker*>( pParameter );

    pInstance->ServiceAllCommands();
}
}
