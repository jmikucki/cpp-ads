#include "DataProviderIf.hpp"
#include "MemoryDataProvider.hpp"
#include "OsInterprocess.hpp"

#include <cstdint>
#include <cstring> // For size_t

#ifndef PROCESSIMAGE_HPP
#define PROCESSIMAGE_HPP

namespace CppAds
{
class ProcessImage
{
  public:
    ProcessImage();
    ~ProcessImage();

    bool RegisterDataProvider( DataProviderIf* pProvider );
    bool ReadByte( uint32_t group, uint32_t offset, uint8_t& dataByte );
    bool WriteByte( uint32_t group, uint32_t offset, uint8_t dataByte );

  private:
    DataProviderIf* GetProviderByIndex( size_t group, size_t offset,
                                        DataProviderIf::AccessType accessType );
    static const size_t MAX_INDEXES = 32U;

    DataProviderIf* m_ProcessDataProvidersRead[MAX_INDEXES]  = {};
    DataProviderIf* m_ProcessDataProvidersWrite[MAX_INDEXES] = {};

    size_t m_FirstEmptyRead;
    size_t m_FirstEmptyWrite;
    Osal::OsMutexType m_Lock;
};
}

#endif // PROCESSIMAGE_HPP
