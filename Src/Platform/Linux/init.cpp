#include "CppAdsApp.hpp"
#include "OsAssert.hpp"

//#include <iostream>
#include <new>

int main()
{
    // std::cout << "Running Ads test implementation" << std::endl;

    CppAds::CppAdsApp adsApp;
    Osal::OsAssert( adsApp.Initialize() == true );
    adsApp.Run();
}
