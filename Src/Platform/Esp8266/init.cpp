/* A very basic C++ example, really just proof of concept for C++

   This sample code is in the public domain.
 */
#include "FreeRTOS.h"
#include "espressif/esp_common.h"
// clang-format off
#define _Static_assert(x,y) static_assert(x, y)
// clang-format on
extern "C" {
#include "espressif/phy_info.h"
}

#include "queue.h"
#include "task.h"

extern "C" {
//#include <esp/uart.h>
}

#include "CppAdsApp.hpp"
#include "OsAssert.hpp"

extern "C" void task1( void* pvParameters )
{
    printf( "Task 1 runing.\n" );

    // sdk_phy_info_t phy_info;
    // printf( "Dump default rc callibration data: \n\n" );
    // get_sdk_default_phy_info( &phy_info );
    // dump_phy_info( &phy_info, false );
    //
    // // printf( "Store default rc calibration data: \n\n" );
    // // write_saved_phy_info( &phy_info );
    //
    // printf( "Dump stored rc callibration data: \n\n" );
    // read_saved_phy_info( &phy_info );
    // dump_phy_info( &phy_info, false );
    //
    // Osal::OsTaskDelayMs( 2500 );

    sdk_wifi_set_phy_mode( PHY_MODE_11G );
    if ( sdk_wifi_set_opmode( STATION_MODE ) != true )
    {
        printf( "Couldn't set op mode.\n" );
    }

    // Create connection structure
    struct sdk_station_config config;

    const char* pSsid  = "Omniscient";
    const char* pPassw = "wakacjewkrzywymzwierciadle";
    // const char* pSsid  = "XT1021";
    // const char* pPassw = "abcdefgh";
    memset( &config.ssid, 0, 32 );
    memset( &config.password, 0, 64 );
    memcpy( &config.ssid, pSsid, strlen( pSsid ) );
    memcpy( &config.password, pPassw, strlen( pPassw ) );
    config.bssid_set = 0;
    memset( &config.bssid, 6, 0 );

    if ( sdk_wifi_station_set_config( &config ) != true )
    {
        printf( "Couldn't set wifi station config.\n" );
    }

    Osal::OsTaskDelayMs( 1000 );
    // sdk_wifi_station_connect();
    printf( "Set station complete.\n" );
    Osal::OsTaskDelayMs( 1000 );

    struct ip_info ip_config;
    printf( "Check if we have ip.\n" );
    sdk_wifi_get_ip_info( STATION_IF, &ip_config );
    while ( ip_config.ip.addr == 0 )
    {
        printf( "No, try again.\n" );
        Osal::OsTaskDelayMs( 1000 );
        sdk_wifi_get_ip_info( STATION_IF, &ip_config );
    }

    printf( "Creating application object.\n" );
    CppAds::CppAdsApp* pApp = new CppAds::CppAdsApp();

    Osal::OsAssert( pApp != nullptr );

    printf( "Initializing application object.\n" );
    pApp->Initialize();
    printf( "Running application object.\n" );
    pApp->Run();

    while ( true )
    {
        Osal::OsTaskDelayMs( 100 );
    }
}

extern "C" void user_init( void )
{
    // sdk_dbg_stop_hw_wdt();
    printf( "CppAds: SDK version:%s\n", sdk_system_get_sdk_version() );
    xTaskCreate( task1, "MainTask", 1024, NULL, 1, NULL );
}

extern "C" void __cxa_pure_virtual()
{
    Osal::OsAssert( false );
}
