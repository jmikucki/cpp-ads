#include "Logger.hpp"

#include <iomanip>
////#include <iostream>
#include "espressif/esp_common.h"
#include <string>

namespace CppAds
{
Logger* Logger::m_pLogger = NULL;

Logger::Logger()
{
}

Logger::~Logger()
{
}

Logger* Logger::GetInstance()
{
    if ( m_pLogger == NULL )
    {
        m_pLogger = new Logger();
    }

    return m_pLogger;
}

void Logger::Log( const char* text )
{
    printf( "%s", text );
    // std::cout << text << std::endl;
}
}
