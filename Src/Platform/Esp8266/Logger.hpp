#ifndef LOGGER_WORKER
#define LOGGER_WORKER

#include <cstdint>
#include <string>

namespace CppAds
{

class Logger
{
  public:
    Logger();
    ~Logger();

    static Logger* GetInstance();

    // void Log( std::string text );
    void Log( const char* text );
    void LogBuffer( uint8_t* pBuffer, size_t length );

  private:
    static Logger* m_pLogger;
};
}

#endif
